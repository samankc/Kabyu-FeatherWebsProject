import 'package:flutter/material.dart';
import 'package:get/get.dart';

import 'package:kabyu_feather_webs/views/2.%20ExplorePage/Explore.dart';
import 'package:kabyu_feather_webs/views/3.%20ChatPage/Chat.dart';
import 'package:kabyu_feather_webs/views/Product%20Individual/product_ind_add.dart';
import 'package:kabyu_feather_webs/views/Product%20Individual/product_individual.dart';

import 'package:kabyu_feather_webs/views/Profile/profile_edit.dart';


void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return GetMaterialApp(
      debugShowCheckedModeBanner: false,
      home: MyHomePage(),
    );
  }
}

class MyHomePage extends StatefulWidget {
  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  int _selectedIndex = 0;
  final List<Widget> _widgetOptions = [
    ProfileEdit(),
    ExplorePage(),
    ChatPage(),
  ];

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        resizeToAvoidBottomInset: false,
        body: _widgetOptions.elementAt(_selectedIndex),
        bottomNavigationBar: BottomNavigationBar(
          items: const <BottomNavigationBarItem>[
            BottomNavigationBarItem(
                icon: Icon(Icons.favorite), title: Text("Wishlist")),
            BottomNavigationBarItem(
                icon: Icon(Icons.explore_outlined), title: Text("Explore")),
            BottomNavigationBarItem(
                icon: Icon(Icons.message), title: Text("Chat")),
          ],
          currentIndex: _selectedIndex,
          selectedItemColor: Color(0xff23036A),
          unselectedItemColor: Color(0xff985EFF),
          onTap: _onItemTapped,
        ),
      ),
    );
  }
}
// import 'package:expandable/expandable.dart';
// import 'package:flutter/material.dart';
// import 'dart:math' as math;

// void main() => runApp(MyApp());

// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return MaterialApp(
//       title: 'Expandable Demo',
//       theme: ThemeData(
//         primarySwatch: Colors.blue,
//       ),
//       home: MyHomePage(),
//     );
//   }
// }

// class MyHomePage extends StatefulWidget {
//   @override
//   State createState() {
//     return MyHomePageState();
//   }
// }

// class MyHomePageState extends State<MyHomePage> {
//   @override
//   Widget build(BuildContext context) {
//     return Scaffold(
//       appBar: AppBar(
//         title: Text("Expandable Demo"),
//       ),
//       body: ExpandableTheme(
//         data: const ExpandableThemeData(
//           iconColor: Colors.blue,
//           useInkWell: true,
//         ),
//         child: ListView(
//           physics: const BouncingScrollPhysics(),
//           children: <Widget>[
//             Card1(),
//             Card2(),
//             Container(width: 100, child: Card3()),
//           ],
//         ),
//       ),
//     );
//   }
// }

// const loremIpsum =
//     "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.";

// class Card1 extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return ExpandableNotifier(
//         child: Padding(
//       padding: const EdgeInsets.all(10),
//       child: Card(
//         clipBehavior: Clip.antiAlias,
//         child: Column(
//           children: <Widget>[
//             SizedBox(
//               height: 150,
//               child: Container(
//                 decoration: BoxDecoration(
//                   color: Colors.orange,
//                   shape: BoxShape.rectangle,
//                 ),
//               ),
//             ),
//             ScrollOnExpand(
//               scrollOnExpand: true,
//               scrollOnCollapse: false,
//               child: ExpandablePanel(
//                 theme: const ExpandableThemeData(
//                   headerAlignment: ExpandablePanelHeaderAlignment.center,
//                   tapBodyToCollapse: true,
//                 ),
//                 header: Padding(
//                     padding: EdgeInsets.all(10),
//                     child: Text(
//                       "ExpandablePanel",
//                       style: Theme.of(context).textTheme.body2,
//                     )),
//                 collapsed: Text(
//                   loremIpsum,
//                   softWrap: true,
//                   maxLines: 2,
//                   overflow: TextOverflow.ellipsis,
//                 ),
//                 expanded: Column(
//                   crossAxisAlignment: CrossAxisAlignment.start,
//                   children: <Widget>[
//                     for (var _ in Iterable.generate(5))
//                       Padding(
//                           padding: EdgeInsets.only(bottom: 10),
//                           child: Text(
//                             loremIpsum,
//                             softWrap: true,
//                             overflow: TextOverflow.fade,
//                           )),
//                   ],
//                 ),
//                 builder: (_, collapsed, expanded) {
//                   return Padding(
//                     padding: EdgeInsets.only(left: 10, right: 10, bottom: 10),
//                     child: Expandable(
//                       collapsed: collapsed,
//                       expanded: expanded,
//                       theme: const ExpandableThemeData(crossFadePoint: 0),
//                     ),
//                   );
//                 },
//               ),
//             ),
//           ],
//         ),
//       ),
//     ));
//   }
// }

// class Card2 extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     buildImg(Color color, double height) {
//       return SizedBox(
//           height: height,
//           child: Container(
//             decoration: BoxDecoration(
//               color: color,
//               shape: BoxShape.rectangle,
//             ),
//           ));
//     }

//     buildCollapsed1() {
//       return Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: <Widget>[
//             Padding(
//               padding: EdgeInsets.all(10),
//               child: Column(
//                 crossAxisAlignment: CrossAxisAlignment.start,
//                 children: <Widget>[
//                   Text(
//                     "Expandable",
//                     style: Theme.of(context).textTheme.body1,
//                   ),
//                 ],
//               ),
//             ),
//           ]);
//     }

//     buildCollapsed2() {
//       return buildImg(Colors.lightGreenAccent, 150);
//     }

//     buildCollapsed3() {
//       return Container();
//     }

//     buildExpanded1() {
//       return Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: <Widget>[
//             Padding(
//               padding: EdgeInsets.all(10),
//               child: Column(
//                 crossAxisAlignment: CrossAxisAlignment.start,
//                 children: <Widget>[
//                   Text(
//                     "Expandable",
//                     style: Theme.of(context).textTheme.body1,
//                   ),
//                   Text(
//                     "3 Expandable widgets",
//                     style: Theme.of(context).textTheme.caption,
//                   ),
//                 ],
//               ),
//             ),
//           ]);
//     }

//     buildExpanded2() {
//       return Column(
//         crossAxisAlignment: CrossAxisAlignment.start,
//         children: <Widget>[
//           Row(
//             children: <Widget>[
//               Expanded(child: buildImg(Colors.lightGreenAccent, 100)),
//               Expanded(child: buildImg(Colors.orange, 100)),
//             ],
//           ),
//           Row(
//             children: <Widget>[
//               Expanded(child: buildImg(Colors.lightBlue, 100)),
//               Expanded(child: buildImg(Colors.cyan, 100)),
//             ],
//           ),
//         ],
//       );
//     }

//     buildExpanded3() {
//       return Padding(
//         padding: EdgeInsets.all(10),
//         child: Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           children: <Widget>[
//             Text(
//               loremIpsum,
//               softWrap: true,
//             ),
//           ],
//         ),
//       );
//     }

//     return ExpandableNotifier(
//         child: Padding(
//       padding: const EdgeInsets.only(left: 10, right: 10, bottom: 10),
//       child: ScrollOnExpand(
//         child: Card(
//           clipBehavior: Clip.antiAlias,
//           child: Column(
//             crossAxisAlignment: CrossAxisAlignment.start,
//             children: <Widget>[
//               Expandable(
//                 collapsed: buildCollapsed1(),
//                 expanded: buildExpanded1(),
//               ),
//               Expandable(
//                 collapsed: buildCollapsed2(),
//                 expanded: buildExpanded2(),
//               ),
//               Expandable(
//                 collapsed: buildCollapsed3(),
//                 expanded: buildExpanded3(),
//               ),
//               Divider(
//                 height: 1,
//               ),
//               Row(
//                 mainAxisAlignment: MainAxisAlignment.start,
//                 children: <Widget>[
//                   Builder(
//                     builder: (context) {
//                       var controller = ExpandableController.of(context);
//                       return FlatButton(
//                         child: Text(
//                           controller.expanded ? "COLLAPSE" : "EXPAND",
//                           style: Theme.of(context)
//                               .textTheme
//                               .button
//                               .copyWith(color: Colors.deepPurple),
//                         ),
//                         onPressed: () {
//                           controller.toggle();
//                         },
//                       );
//                     },
//                   ),
//                 ],
//               ),
//             ],
//           ),
//         ),
//       ),
//     ));
//   }
// }

// class Card3 extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     buildItem(String label) {
//       return Padding(
//         padding: const EdgeInsets.all(10.0),
//         child: Container(child: Text(label)),
//       );
//     }

//     buildList() {
//       return GridView.count(
//         shrinkWrap: true,
//         primary: true,
//         crossAxisCount: 2,
//         children: <Widget>[
//           RaisedButton(
//             onPressed: () {},
//             child: Text("Hello"),
//           ),
//           RaisedButton(
//             onPressed: () {},
//             child: Text("Hello"),
//           ),
//           RaisedButton(
//             onPressed: () {},
//             child: Text("Hello"),
//           ),
//           RaisedButton(
//             onPressed: () {},
//             child: Text("Hello"),
//           )
//         ],
//       );
//     }

//     return ExpandableNotifier(
//         child: Padding(
//       padding: const EdgeInsets.all(10),
//       child: ScrollOnExpand(
//         child: ExpandablePanel(
//           theme: const ExpandableThemeData(
//             headerAlignment: ExpandablePanelHeaderAlignment.center,
//             tapBodyToExpand: true,
//             tapBodyToCollapse: true,
//             hasIcon: false,
//           ),
//           header: Container(
//             color: Colors.indigoAccent,
//             child: Padding(
//               padding: const EdgeInsets.all(10.0),
//               child: Row(
//                 children: [
//                   Expanded(
//                     child: Text(
//                       "Items",
//                       style: Theme.of(context)
//                           .textTheme
//                           .body2
//                           .copyWith(color: Colors.white),
//                     ),
//                   ),
//                   ExpandableIcon(
//                     theme: const ExpandableThemeData(
//                       expandIcon: Icons.arrow_right,
//                       collapseIcon: Icons.arrow_drop_down,
//                       iconColor: Colors.white,
//                       iconSize: 28.0,
//                       iconRotationAngle: math.pi / 2,
//                       iconPadding: EdgeInsets.only(right: 5),
//                       hasIcon: false,
//                     ),
//                   ),
//                 ],
//               ),
//             ),
//           ),
//           expanded: buildList(),
//         ),
//       ),
//     ));
//   }
// }
// import 'package:flutter/material.dart';
// import 'package:kabyu_feather_webs/Model/category_model.dart';
// import 'package:meta/meta.dart';

// void main() => runApp(new MyApp());

// class MyApp extends StatelessWidget {
//   @override
//   Widget build(BuildContext context) {
//     return new MaterialApp(
//       title: 'Flutter Demo',
//       home: new HomeScreen(),
//     );
//   }
// }

// class HomeScreen extends StatelessWidget {
//   final String description =
//       "Flutter is Google’s mobile UI framework for crafting high-quality native interfaces on iOS and Android in record time. Flutter works with existing code, is used by developers and organizations around the world, and is free and open source.";
//   CategoryModel category;

//   @override
//   Widget build(BuildContext context) {
//     print(category.id);
//     return new Scaffold(
//       appBar: new AppBar(
//         title: const Text("Demo App"),
//       ),

//       body: DescriptionTextWidget(text: category),
//     );
//   }
// }

// class DescriptionTextWidget extends StatefulWidget {
//   final CategoryModel text;

//   DescriptionTextWidget({@required this.text});

//   @override
//   _DescriptionTextWidgetState createState() =>
//       new _DescriptionTextWidgetState();
// }

// class _DescriptionTextWidgetState extends State<DescriptionTextWidget> {
//   String firstHalf;
//   String secondHalf;
//   CategoryModel category;
//   bool flag = true;

//   @override
//   void initState() {
//     super.initState();
//     print(category.id);
//     if (category.id <= 5) {
//       print(category.id);
//       for (var i = 0; i <= 5; i++) {
//         Container(
//           child: ButtonTheme(
//             minWidth: 100,
//             height: 30,
//             child: RaisedButton(
//               onPressed: () {},
//               child: Text(category.title),
//             ),
//           ),
//         );
//       }
//       secondHalf = widget.text.title;
//     } else {
//       firstHalf = widget.text.title;
//       secondHalf = "";
//     }
//   }

//   @override
//   Widget build(BuildContext context) {
//     return new Container(
//       padding: new EdgeInsets.symmetric(horizontal: 10.0, vertical: 10.0),
//       child: secondHalf.isEmpty
//           ? new Text(firstHalf)
//           : new Column(
//               children: <Widget>[
//                 new Text(flag ? (firstHalf + "...") : (firstHalf + secondHalf)),
//                 new Container(
//                   child: new Row(
//                     mainAxisAlignment: MainAxisAlignment.end,
//                     children: <Widget>[
//                       new RaisedButton(
//                         child: Text(flag ? "show more" : "show less"),
//                         color: Colors.blue,
//                         onPressed: () {
//                           setState(() {
//                             flag = !flag;
//                           });
//                         },
//                       ),
//                     ],
//                   ),
//                 ),
//               ],
//             ),
//     );
//   }
// }

// class ExplorePage extends StatefulWidget {
//   static const String id = 'explorePage';

//   @override
//   _ExplorePageState createState() => _ExplorePageState();
// }

// class _ExplorePageState extends State<ExplorePage> {
//   bool _isVisibleCategory = false;
//   void showExtraCategory() {
//     setState(() {
//       _isVisibleCategory = !_isVisibleCategory;
//     });
//   }

//   @override
//   Widget build(BuildContext context) {
//     return buildCategory();
//   }

//   Widget buildCategory() {
//     return Container(
//       child: Column(
//         children: [
//           buildRowCategory(
//               name1: "Science",
//               name2: "Psychlog",
//               name3: "Thriller",
//               color1: Color(0xFFF2E7FE),
//               color2: Color(0xFFF2E7FE),
//               textcolor: Colors.black,
//               onpressed: () {}),
//           buildRowCategory(
//               name1: "Drama",
//               name2: "Romance",
//               name3: _isVisibleCategory ? "Less " : "More ",
//               color1: Color(0xFFF2E7FE),
//               color2: Color(0xFF3700B3),
//               textcolor: Colors.white,
//               icon: _isVisibleCategory ? Icons.remove : Icons.add,
//               onpressed: () {}),
//           // Visibility(
//           //   visible: _isVisibleCategory,
//           //   child: Column(children: [
//           //     buildRowCategory(
//           //         name1: "Science",
//           //         name2: "Psychlog",
//           //         name3: "Thriller",
//           //         color1: Color(0xFFF2E7FE),
//           //         color2: Color(0xFFF2E7FE),
//           //         textcolor: Colors.black,
//           //         onpressed: () {}),
//           //     buildRowCategory(
//           //         name1: "Drama",
//           //         name2: "Romance",
//           //         name3: "Science",
//           //         color1: Color(0xFFF2E7FE),
//           //         color2: Color(0xFFF2E7FE),
//           //         textcolor: Colors.black,
//           //         onpressed: () {}),
//           //   ]),
//           // ),
//         ],
//       ),
//     );
//   }

//   Widget buildRowCategory(
//       {String name1,
//       String name2,
//       String name3,
//       Color color1,
//       Color color2,
//       Color textcolor,
//       IconData icon,
//       Function onpressed}) {
//     return Row(
//       children: [
//         Expanded(
//             child: Row(
//           mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//           children: [
//             Container(
//               child: ButtonTheme(
//                 minWidth: 100,
//                 height: 30,
//                 child: RaisedButton(
//                   onPressed: () {},
//                   child: Text(name1),
//                   color: color1,
//                 ),
//               ),
//             ),
//             Container(
//               child: ButtonTheme(
//                 minWidth: 100,
//                 height: 30,
//                 child: RaisedButton(
//                   onPressed: () {},
//                   child: Text(name2),
//                   color: color1,
//                 ),
//               ),
//             ),
//             Container(
//               child: ButtonTheme(
//                 minWidth: 100,
//                 height: 30,
//                 child: RaisedButton(
//                   onPressed: onpressed,
//                   child: Row(
//                     children: [
//                       Text(
//                         name3,
//                         style: TextStyle(color: textcolor),
//                       ),
//                       Icon(
//                         icon,
//                         size: 15,
//                         color: Colors.white,
//                       ),
//                     ],
//                   ),
//                   color: color2,
//                 ),
//               ),
//             ),
//           ],
//         )),
//       ],
//     );
//   }
// }
