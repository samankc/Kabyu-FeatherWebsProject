import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:kabyu_feather_webs/views/Category/booksbasedon_category.dart';
import 'package:kabyu_feather_webs/views/Error%20Page/error_network.dart';

class TopNavigationBar extends StatelessWidget {
  final IconData icon;
  final Function ontap;

  const TopNavigationBar({Key key, this.icon, this.ontap}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 12, vertical: 12),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          InkWell(
            onTap: ontap,
            child: Icon(icon),
          ),
          Expanded(
            child: Padding(
              padding: const EdgeInsets.only(left: 5),
              child: Container(
                height: 37,
                decoration: BoxDecoration(
                    color: Color(0xffEDEDED),
                    borderRadius: BorderRadius.all(Radius.circular(25))),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 15),
                        child: TextField(
                          onChanged: (value) {
                            // password = value;
                          },
                          decoration: InputDecoration(
                            hintText: "Search",
                            hintStyle: TextStyle(
                                fontSize: 18, color: Color(0xff939393)),
                            // fillColor: Colors.white,
                            border: InputBorder.none,
                          ),
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(right: 15),
                      child: InkWell(
                        onTap: () {
                          print("Pressed");
                        },
                        child: Icon(
                          Icons.search,
                          size: 30,
                          color: Color(0xff30009C),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(left: 8),
            child: GestureDetector(
              child: Icon(
                Icons.notifications,
                color: Color(0xff30009C),
                size: 30,
              ),
              onTap: () {
                // Navigator.push(
                //   context,
                //   MaterialPageRoute(builder: (context) => ErrorNetwork()),
                // );
                Get.to(BooksBasedOnCategory());
              },
            ),
          )
        ],
      ),
    );
  }
}
