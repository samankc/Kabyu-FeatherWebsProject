import 'dart:io';

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:kabyu_feather_webs/widgets/image_picker.dart';

//Square Image Picker Box for product individual add page
class ImagePickSquare extends StatefulWidget {
  @override
  _ImagePickSquareState createState() => _ImagePickSquareState();
}

final List<String> imgList = [
  'https://images.unsplash.com/photo-1589829085413-56de8ae18c73?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1086&q=80',
  'https://images.unsplash.com/photo-1589829085413-56de8ae18c73?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1086&q=80',
  'https://images.unsplash.com/photo-1589829085413-56de8ae18c73?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1086&q=80',
  'https://images.unsplash.com/photo-1589829085413-56de8ae18c73?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1086&q=80',
  'https://images.unsplash.com/photo-1589829085413-56de8ae18c73?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1086&q=80',
  'https://images.unsplash.com/photo-1589829085413-56de8ae18c73?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=1086&q=80',
];

class _ImagePickSquareState extends State<ImagePickSquare> {
  File _image;
  _imgFromCamera() async {
    File image = await ImagePicker.pickImage(
        source: ImageSource.camera, imageQuality: 50);

    setState(() {
      _image = image;
    });
  }

  _imgFromGallery() async {
    File image = await ImagePicker.pickImage(
        source: ImageSource.gallery, imageQuality: 50);

    setState(() {
      _image = image;
    });
  }

  //showing picker from the bottomsheet
  void _showPicker(context) {
    showModalBottomSheet(
        context: context,
        builder: (BuildContext bc) {
          return SafeArea(
            child: Container(
              child: new Wrap(
                children: <Widget>[
                  new ListTile(
                      leading: new Icon(Icons.photo_library),
                      title: new Text('Photo Library'),
                      onTap: () {
                        _imgFromGallery();
                        Navigator.of(context).pop();
                      }),
                  new ListTile(
                    leading: new Icon(Icons.photo_camera),
                    title: new Text('Camera'),
                    onTap: () {
                      _imgFromCamera();
                      Navigator.of(context).pop();
                    },
                  ),
                ],
              ),
            ),
          );
        });
  }

  final List<Widget> imageSliders = imgList
      .map((item) => AnimatedContainer(
            duration: Duration(seconds: 1),
            child: Container(
                decoration: BoxDecoration(
                  color: Colors.black38,
                  // borderRadius: BorderRadius.circular(75)),
                ),
                width: 160,
                height: 170,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Container(
                      height: 75,
                      child: Text("A",
                          style: TextStyle(
                            color: Colors.black38,
                            fontSize: 72,
                            fontWeight: FontWeight.w500,
                          )),
                    ),
                    Container(
                      width: 150,
                      height: 45,
                      decoration: BoxDecoration(
                        color: Colors.black38,
                      ),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Icon(
                            Icons.camera_alt,
                            color: Colors.black38,
                            size: 16,
                          ),
                          Text("Upload photo",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 12,
                              )),
                          SizedBox(height: 15)
                        ],
                      ),
                    )
                  ],
                )),
          ))
      .toList();

  //design for square box
  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: () {
        ImagePickerWidget();
        print("pressed");
      },
      child: Container(
        // decoration: BoxDecoration(borderRadius: BorderRadius.circular(90)),
        child: _image != null
            ? Container(
                // borderRadius: BorderRadius.circular(75),
                child: Image.file(
                  _image,
                  width: 150,
                  height: 180,
                  fit: BoxFit.fitHeight,
                ),
              )
            : Container(
                decoration: BoxDecoration(
                  color: Colors.black38,
                  // borderRadius: BorderRadius.circular(75)),
                ),
                width: 150,
                height: 150,
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.end,
                  children: [
                    Container(
                      height: 75,
                      child: Text("A",
                          style: TextStyle(
                            color: Colors.black38,
                            fontSize: 72,
                            fontWeight: FontWeight.w500,
                          )),
                    ),
                    Container(
                      width: 150,
                      height: 75,
                      decoration: BoxDecoration(
                          color: Colors.black38,
                          borderRadius: BorderRadius.only(
                            bottomLeft: Radius.circular(150),
                            bottomRight: Radius.circular(150),
                          )),
                      child: Column(
                        mainAxisAlignment: MainAxisAlignment.end,
                        children: [
                          Icon(
                            Icons.camera_alt,
                            color: Colors.black38,
                            size: 16,
                          ),
                          Text("Upload photo",
                              style: TextStyle(
                                color: Colors.white,
                                fontSize: 12,
                              )),
                          SizedBox(height: 15)
                        ],
                      ),
                    )
                  ],
                )),
      ),
    );
  }
}
